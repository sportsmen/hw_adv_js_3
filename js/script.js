function createTable() {
    const fragment = document.createDocumentFragment();
    const table = document.createElement('table');
    table.id = 'table';
    fragment.appendChild(table);
    for (let i = 0; i < 30; i++){
        let row = document.createElement('tr');
        for (let j = 0; j < 30; j++) {
            let sell = document.createElement('td');
            sell.className = 'item';
            row.appendChild(sell);
        }
        table.appendChild(row);
    }
    document.body.appendChild(fragment);
}
document.addEventListener('click',  (e) => {
    let target = e.target;
    if (target.tagName === 'BODY') {
        document.getElementById('table').classList.toggle('inverted-table');
        document.querySelectorAll('.checked').forEach((sell) =>  sell.classList.toggle('inverted'));
        return;
    }
    while (target !== document.getElementById('table')) {
        if (target && target.classList.contains('item')) {
            target.classList.toggle('checked');
            if (document.getElementById('table').classList.contains('inverted-table'))  target.classList.toggle('inverted');
            return;
        }
        target = target.parentNode;
    }
});
createTable();